package com.example.iot_smartlock;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.iot_smartlock.FaceDetection.Box;
import com.example.iot_smartlock.FaceDetection.MTCNN;
import com.example.iot_smartlock.FaceRecognition.FaceNet;

import java.io.IOException;
import java.util.LinkedList;
import java.util.Vector;

public class MainActivity extends AppCompatActivity {
    private static int REQUEST_IMAGE_CAPTURE = 0;
    private static int PROCESS_SETUP_FACES = 0;
    private static int PROCESS_OPEN_LOCK = 0;

    public static class Face {
        private final float[][] faceCaracteristics;
        private final Bitmap faceImage;
        private final String faceName;
        private double faceScore;

        public Face(float[][] faceCaracteristics, Bitmap faceImage, String faceName) {
            this.faceCaracteristics = faceCaracteristics;
            this.faceImage          = faceImage;
            this.faceName           = faceName;
        }

        public Face(float[][] faceCaracteristics, Bitmap faceImage) {
            this.faceCaracteristics = faceCaracteristics;
            this.faceImage          = faceImage;
            this.faceName           = null;
        }
    }

    private final LinkedList<Bitmap> facesForProcess = new LinkedList<>();
    private final LinkedList<Face> faces = new LinkedList<>();

    // Instantiate the RequestQueue.
    RequestQueue queue;
    String url = "http://192.168.0.128";

    private MTCNN   mtcnn   = null;
    private FaceNet facenet = null;

    private Bitmap cropFace(Bitmap bitmap){
        Bitmap croppedBitmap = null;
        try {
            Vector<Box> boxes = mtcnn.detectFaces(bitmap, 10);

            Log.i("MTCNN", "No. of faces detected: " + boxes.size());

            int x = boxes.get(0).left();
            int y = boxes.get(0).top();
            int width = boxes.get(0).width();
            int height = boxes.get(0).height();


            if (y + height >= bitmap.getHeight())
                height -= (y + height) - (bitmap.getHeight() - 1);
            if (x + width >= bitmap.getWidth())
                width -= (x + width) - (bitmap.getWidth() - 1);

            Log.i("MTCNN", "Final x: " + (x + width));
            Log.i("MTCNN", "Width: " + bitmap.getWidth());
            Log.i("MTCNN", "Final y: " + (y + width));
            Log.i("MTCNN", "Height: " + bitmap.getWidth());

            croppedBitmap = Bitmap.createBitmap(bitmap, x, y, width, height);
        }catch (Exception e){
            e.printStackTrace();
        }
        return croppedBitmap;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button openLockButton = findViewById(R.id.button);
        Button closeLockButton = findViewById(R.id.button3);
        Button setupFacesButton = findViewById(R.id.button2);
        queue = Volley.newRequestQueue(getApplicationContext());

        try {
            mtcnn = new MTCNN(getAssets());
            facenet = new FaceNet(getAssets());
        } catch (IOException e) {
            e.printStackTrace();
        }

        setupFacesButton.setOnClickListener(view -> {
            REQUEST_IMAGE_CAPTURE = 1;
            PROCESS_SETUP_FACES = 1;
            dispatchTakePictureIntent();
        });

        closeLockButton.setOnClickListener(view -> {
            Toast.makeText(getApplicationContext(), "Closing Door", Toast.LENGTH_SHORT).show();
            String auxUrl = url + "/relay/on"; //TODO add here the request to ESP32 (the remaining part of the url)
            Log.i("DEBUG", auxUrl);
            sendHttpRequest(auxUrl);
        });

        openLockButton.setOnClickListener(view -> {
            REQUEST_IMAGE_CAPTURE = 1;
            PROCESS_OPEN_LOCK = 1;
            dispatchTakePictureIntent();
        });
    }
    private void sendHttpRequest(String auxUrl) { //auxUrl = url + "relay/on" or "relay/off"
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, auxUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        Toast.makeText(getApplicationContext(), "Success", Toast.LENGTH_SHORT).show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERROR", error.getMessage());
                Toast.makeText(getApplicationContext(), "ERROR while sending the request", Toast.LENGTH_SHORT).show();
            }
        });

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        
        try {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(getApplicationContext(), "ERROR: Could not take picture.", Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Log.i("USER", "Got a picture to process");
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            Bitmap auxBitmap = cropFace(imageBitmap);
            if (auxBitmap != null) {
                facesForProcess.add(auxBitmap);
                REQUEST_IMAGE_CAPTURE = 0;


                if (PROCESS_SETUP_FACES == 1) {
                    faces.clear();
                    faces.add(new Face(facenet.run(facesForProcess.poll()), imageBitmap));
                    PROCESS_SETUP_FACES = 0;
                }
                if (PROCESS_OPEN_LOCK == 1) {
                    faces.getFirst().faceScore = 100;
                    float[][] faceToCompareCaracteristics = facenet.run(facesForProcess.poll());
                    faces.getFirst().faceScore = facenet.getSimilarityScore(faceToCompareCaracteristics, faces.getFirst().faceCaracteristics);

                    Log.i("USER", "Face score: " + faces.getFirst().faceScore);
                    if (faces.getFirst().faceScore > 10)
                        Toast.makeText(getApplicationContext(), "Your face is not recognised", Toast.LENGTH_SHORT).show();
                    else {
                        Toast.makeText(getApplicationContext(), "Your face is recognised. Opening lock.", Toast.LENGTH_SHORT).show();
                        String auxUrl = url + "/relay/off"; //TODO add here the request to ESP32 (the remaining part of the url)
                        Log.i("DEBUG", auxUrl);
                        sendHttpRequest(auxUrl);
                    }
                    PROCESS_OPEN_LOCK = 0;
                }
            } else {
                Toast.makeText(getApplicationContext(), "ERROR: Could not find face.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        mtcnn.close();
        facenet.close();
    }
}
