#include "WiFi.h"
#include "ESPAsyncWebServer.h"

const char* ssid = "TP-Link_ACC9";
const char* password =  "pas";

AsyncWebServer server(80);
int RELAY_PIN = 27;

void setup() {
 pinMode(RELAY_PIN, OUTPUT);
 digitalWrite(RELAY_PIN, HIGH);
 Serial.begin(115200);
 WiFi.begin(ssid, password);
 
 while (WiFi.status() != WL_CONNECTED) {
   delay(1000);
   Serial.println("Connecting to WiFi..");
 }
 
 Serial.println(WiFi.localIP());
 
 server.on("/relay/off", HTTP_GET   , [](AsyncWebServerRequest *request){
   request->send(200, "text/plain", "ok");
   digitalWrite(RELAY_PIN, HIGH);
 });
  server.on("/relay/on", HTTP_GET, [](AsyncWebServerRequest *request){
   request->send(200, "text/plain","ok");
   digitalWrite(RELAY_PIN, LOW);
 });
 
 server.begin();
}
void loop(){}
